module.exports = ({ env }) => ({
  defaultConnection: "default",
  connections: {
    default: {
      //connector: "strapi-mongoose",
      settings: {
        client: "postgres",
        uri: "${process.env.DATABASE_URI || ''}",
        host: "${process.env.DATABASE_HOST || 'dpg-c27idvgfpg6i1utgkadg'}",
        port: "${process.env.DATABASE_PORT || 27017}",
        database: "${process.env.DATABASE_NAME || 'apiaraf2021'}",
        username: "${process.env.DATABASE_USERNAME || 'apiaraf2021'}",
        password: "${process.env.DATABASE_PASSWORD || 'apiaraf2021'}"
      },
      options: {
        ssl: "${process.env.DATABASE_SSL || false}",
        authenticationDatabase: "${process.env.DATABASE_AUTHENTICATION_DATABASE || ''}"
      }
    }
  }
});
